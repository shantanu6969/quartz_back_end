package com.deepiq.quartzdemo.model;

import java.time.LocalDateTime;

public class ScheduleJobRequest {

	private String wellNames;

	private Double startDepth;

	private Double endDepth;

	private LocalDateTime startTimeLog;

	private LocalDateTime endTimeLog;

	private Integer _hour;

	private Integer _minute;

	private String _day;

	private Integer _date;

	private String frequency;

	public String getWellNames() {
		return wellNames;
	}

	public void setWellNames(String wellNames) {
		this.wellNames = wellNames;
	}

	public Double getStartDepth() {
		return startDepth;
	}

	public void setStartDepth(Double startDepth) {
		this.startDepth = startDepth;
	}

	public Double getEndDepth() {
		return endDepth;
	}

	public void setEndDepth(Double endDepth) {
		this.endDepth = endDepth;
	}

	public LocalDateTime getStartTimeLog() {
		return startTimeLog;
	}

	public void setStartTimeLog(LocalDateTime startTimeLog) {
		this.startTimeLog = startTimeLog;
	}

	public LocalDateTime getEndTimeLog() {
		return endTimeLog;
	}

	public void setEndTimeLog(LocalDateTime endTimeLog) {
		this.endTimeLog = endTimeLog;
	}

	public Integer get_hour() {
		return _hour;
	}

	public void set_hour(Integer _hour) {
		this._hour = _hour;
	}

	public Integer get_minute() {
		return _minute;
	}

	public void set_minute(Integer _minute) {
		this._minute = _minute;
	}

	public String get_day() {
		return _day;
	}

	public void set_day(String _day) {
		this._day = _day;
	}

	public Integer get_date() {
		return _date;
	}

	public void set_date(Integer _date) {
		this._date = _date;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

}
