package com.deepiq.quartzdemo.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import com.deepiq.quartzdemo.entity.Job;
import com.deepiq.quartzdemo.repository.JobRepository;

@Component
public class JobComponent extends QuartzJobBean {
    private static final Logger logger = LoggerFactory.getLogger(JobComponent.class);

    @Autowired
    private JobRepository jobRepository;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        logger.info("Executing Job with key {}", jobExecutionContext.getJobDetail().getKey());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        JobDataMap jobDataMap = jobExecutionContext.getMergedJobDataMap();
        
        Job job = new Job();
        job.setWellNames(jobDataMap.getString("wellNames"));
        job.setStartDepth(jobDataMap.getDouble("startDepth"));
        job.setEndDepth(jobDataMap.getDouble("endDepth"));
        
        LocalDateTime startTimeLog = LocalDateTime.parse(jobDataMap.getString("startTimeLog"), formatter);
        LocalDateTime endTimeLog = LocalDateTime.parse(jobDataMap.getString("endTimeLog"), formatter);
        
        job.setStartTimeLog(startTimeLog);
        job.setEndTimeLog(endTimeLog);
        job.setStatus("running");
        
        jobRepository.save(job);
    }

}
