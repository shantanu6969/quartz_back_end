package com.deepiq.quartzdemo.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "job")
public class Job implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "well_names")
	private String wellNames; // well1, well2, well3

	@Column(name = "start_time_log")
	private LocalDateTime startTimeLog;

	@Column(name = "end_time_log")
	private LocalDateTime endTimeLog;

	@Column(name = "start_depth")
	private Double startDepth;

	@Column(name = "end_depth")
	private Double endDepth;

	@Column(name = "status")
	private String status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWellNames() {
		return wellNames;
	}

	public void setWellNames(String wellNames) {
		this.wellNames = wellNames;
	}

	public LocalDateTime getStartTimeLog() {
		return startTimeLog;
	}

	public void setStartTimeLog(LocalDateTime startTimeLog) {
		this.startTimeLog = startTimeLog;
	}

	public LocalDateTime getEndTimeLog() {
		return endTimeLog;
	}

	public void setEndTimeLog(LocalDateTime endTimeLog) {
		this.endTimeLog = endTimeLog;
	}

	public Double getStartDepth() {
		return startDepth;
	}

	public void setStartDepth(Double startDepth) {
		this.startDepth = startDepth;
	}

	public Double getEndDepth() {
		return endDepth;
	}

	public void setEndDepth(Double endDepth) {
		this.endDepth = endDepth;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
