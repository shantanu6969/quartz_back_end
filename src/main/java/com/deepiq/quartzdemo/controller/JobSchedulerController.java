package com.deepiq.quartzdemo.controller;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.UUID;

import javax.validation.Valid;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.deepiq.quartzdemo.model.ScheduleJobRequest;
import com.deepiq.quartzdemo.model.ScheduleJobResponse;
import com.deepiq.quartzdemo.service.JobComponent;

@RestController
public class JobSchedulerController {
	private static final Logger logger = LoggerFactory.getLogger(JobSchedulerController.class);

	@Autowired
	private Scheduler scheduler;

	@Value("${quartz.zoneId}")
	private String zoneId;

	@PostMapping("/api/scheduleJob")
	public ResponseEntity<ScheduleJobResponse> scheduleJob(@Valid @RequestBody ScheduleJobRequest scheduleJobRequest) {
		try {

//			ZonedDateTime dateTime = null;
//			if (scheduleJobRequest.getScheduleTime() != null) {
//				dateTime = ZonedDateTime.of(scheduleJobRequest.getScheduleTime(), ZoneId.of(zoneId));
//				if (dateTime.isBefore(ZonedDateTime.now())) {
//					ScheduleJobResponse scheduleEmailResponse = new ScheduleJobResponse(false,
//							"ScheduleTime must be after current time");
//					return ResponseEntity.badRequest().body(scheduleEmailResponse);
//				}
//			}

			JobDetail jobDetail = buildJobDetail(scheduleJobRequest);
			Trigger trigger = null;
			if (scheduleJobRequest.getFrequency().equals("now")) {
				trigger = fireNow(jobDetail);
			}
			if (scheduleJobRequest.getFrequency().equals("daily")) {
				trigger = fireDaily(jobDetail, scheduleJobRequest);
			}
			if (scheduleJobRequest.getFrequency().equals("weekly")) {
				trigger = fireWeekly(jobDetail, scheduleJobRequest);
			}
			if (scheduleJobRequest.getFrequency().equals("monthly")) {
				trigger = fireMonthly(jobDetail, scheduleJobRequest);
			}

			scheduler.scheduleJob(jobDetail, trigger);

			ScheduleJobResponse scheduleJobResponse = new ScheduleJobResponse(true, jobDetail.getKey().getName(),
					jobDetail.getKey().getGroup(), "Job Scheduled Successfully!");
			return ResponseEntity.ok(scheduleJobResponse);
		} catch (SchedulerException ex) {
			logger.error("Error scheduling job", ex);

			ScheduleJobResponse scheduleJobResponse = new ScheduleJobResponse(false,
					"Error scheduling job. Please try later!");
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(scheduleJobResponse);
		}
	}

	private JobDetail buildJobDetail(ScheduleJobRequest scheduleJobRequest) {
		JobDataMap jobDataMap = new JobDataMap();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

		jobDataMap.put("wellNames", scheduleJobRequest.getWellNames());
		jobDataMap.put("startDepth", scheduleJobRequest.getStartDepth());
		jobDataMap.put("endDepth", scheduleJobRequest.getEndDepth());
		jobDataMap.put("startTimeLog", formatter.format(scheduleJobRequest.getStartTimeLog()));
		jobDataMap.put("endTimeLog", formatter.format(scheduleJobRequest.getEndTimeLog()));

		return JobBuilder.newJob(JobComponent.class).withIdentity(UUID.randomUUID().toString(), "jobs")
				.withDescription("Store Job").usingJobData(jobDataMap).storeDurably().build();
	}

	private Trigger buildJobTrigger(JobDetail jobDetail, ZonedDateTime startAt) {
		return TriggerBuilder.newTrigger().forJob(jobDetail).withIdentity(jobDetail.getKey().getName(), "job-triggers")
				.withDescription("Store Job Trigger").startAt(Date.from(startAt.toInstant()))
				.withSchedule(SimpleScheduleBuilder.simpleSchedule().withMisfireHandlingInstructionFireNow()).build();
	}

	// Fire monthly
	public static Trigger fireMonthly(JobDetail jobDetail, ScheduleJobRequest req) {

		String cronExpression = "0 " + req.get_minute() + " " + req.get_hour() + " "+req.get_date()+" * ? *";

		return TriggerBuilder.newTrigger().forJob(jobDetail).withIdentity(jobDetail.getKey().getName(), "job-triggers")
				.withDescription("Store Job Trigger").withSchedule(CronScheduleBuilder.cronSchedule(cronExpression))
				.build();
	}

	// Fire daily
	public static Trigger fireDaily(JobDetail jobDetail, ScheduleJobRequest req) {

		String cronExpression = "0 " + req.get_minute() + " 0/" + req.get_hour() + " ? * * *";

		return TriggerBuilder.newTrigger().forJob(jobDetail).withIdentity(jobDetail.getKey().getName(), "job-triggers")
				.withDescription("Store Job Trigger").withSchedule(CronScheduleBuilder.cronSchedule(cronExpression))
				.build();
	}

	// Fire weekly
	public static Trigger fireWeekly(JobDetail jobDetail, ScheduleJobRequest req) {

		String cronExpression = "0 " + req.get_minute() + " " + req.get_hour() + " ? * " + req.get_day() + " *";

		return TriggerBuilder.newTrigger().forJob(jobDetail).withIdentity(jobDetail.getKey().getName(), "job-triggers")
				.withDescription("Store Job Trigger").withSchedule(CronScheduleBuilder.cronSchedule(cronExpression))
				.build();
	}

	// Fire Now
	public static Trigger fireNow(JobDetail jobDetail) {

		return TriggerBuilder.newTrigger().forJob(jobDetail).withIdentity(jobDetail.getKey().getName(), "job-triggers")
				.withDescription("Store Job Trigger").startNow().build();
	}
}
