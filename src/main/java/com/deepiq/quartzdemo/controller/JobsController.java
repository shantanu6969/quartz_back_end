package com.deepiq.quartzdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.deepiq.quartzdemo.entity.Job;
import com.deepiq.quartzdemo.model.ApiResponse;
import com.deepiq.quartzdemo.model.UserDto;
import com.deepiq.quartzdemo.repository.JobRepository;

@RestController
@RequestMapping("/api/job")
public class JobsController {

	@Autowired
	private JobRepository jobRepository;

	@PostMapping
	public ApiResponse<Job> saveUser(@RequestBody Job job) {
		return new ApiResponse<>(HttpStatus.OK.value(), "User saved successfully.", jobRepository.save(job));
	}

	@GetMapping
	public ApiResponse<List<Job>> listUser() {
		return new ApiResponse<>(HttpStatus.OK.value(), "Job list fetched successfully.", jobRepository.findAll());
	}

	@GetMapping("/{id}")
	public ApiResponse<Job> getOne(@PathVariable Long id) {
		return new ApiResponse<>(HttpStatus.OK.value(), "Job fetched successfully.",
				jobRepository.findById(id).orElse(null));
	}

	@PutMapping("/{id}")
	public ApiResponse<UserDto> update(@RequestBody Job job) {
		return new ApiResponse<>(HttpStatus.OK.value(), "Job updated successfully.", jobRepository.save(job));
	}

	@DeleteMapping("/{id}")
	public ApiResponse<Void> delete(@PathVariable Long id) {
		jobRepository.deleteById(id);
		return new ApiResponse<>(HttpStatus.OK.value(), "Job fetched successfully.", null);
	}

}