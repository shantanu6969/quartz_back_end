package com.deepiq.quartzdemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.deepiq.quartzdemo.entity.User;
import com.deepiq.quartzdemo.model.ApiResponse;
import com.deepiq.quartzdemo.model.LoginUser;
import com.deepiq.quartzdemo.repository.UserRepository;

@RestController
@RequestMapping("/api")
public class AuthenticationController {

	@Autowired
	private UserRepository userRepository;

	@PostMapping(value = "/authenticate")
	public ApiResponse<User> postJobEntity(@RequestBody LoginUser loginUser) {

		User user = userRepository.findByUsername(loginUser.getUsername());

		if (user != null && user.getPassword().equals(loginUser.getPassword())) {
			return new ApiResponse<>(HttpStatus.OK.value(), "Logged in successfully.", user);
		} else {
			return new ApiResponse<>(HttpStatus.UNAUTHORIZED.value(), "Invalid credentials.", null);
		}
	}
}
